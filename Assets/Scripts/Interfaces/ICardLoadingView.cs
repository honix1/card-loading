﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace CardLoading
{
    public interface ICardLoadingView
    {
        Vector2Int PictureSize { get; }

        event Action<ICardLoader> DropdownChoice;
        event Action ButtonLoadClick;
        event Action ButtonCancelClick;

        void CreateCards(int count);
        CardState GetCardState(int cardIdx);
        void SetButtonCancelInteractable(bool isInteractable);
        void SetCardPictureTexture(int cardIdx, Texture2D texture);
        UniTask SetCardStateAsync(int cardIdx, CardState state, bool animate = true);
        void SetDropdownAndButtonLoadInteractable(bool isInteractable);
        void SetDropdownOptions(IEnumerable<KeyValuePair<string, ICardLoader>> options);
    }
}