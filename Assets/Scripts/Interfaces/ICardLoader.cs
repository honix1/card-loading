﻿using Cysharp.Threading.Tasks;
using System.Threading;

namespace CardLoading
{
    public interface ICardLoader
    {
        string Name { get; }
        UniTask LoadAsync(
            ICardLoadingModel model,
            ICardLoadingView view,
            CancellationToken cancellationToken);
    }
}