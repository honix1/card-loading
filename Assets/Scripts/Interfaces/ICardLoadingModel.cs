﻿using Cysharp.Threading.Tasks;
using System.Collections.Generic;
using System.Threading;

namespace CardLoading
{
    public interface ICardLoadingModel
    {
        int CardsCount { get; }

        IEnumerable<int> AllCards();
        void CancelLoading();
        UniTask LoadCardImageAsync(int cardIdx, CancellationToken cancellationToken);
        UniTask LoadCardsAsync();
        void SetCardLoader(ICardLoader cardLoader);
    }
}