﻿using Cysharp.Threading.Tasks;
using System.Threading;
using UnityEngine;

namespace CardLoading
{
    public interface ITextureLoader
    {
        /// <returns>
        /// Кортеж bool и Texture2D.
        /// bool имеет значение True только в случае успешной загрузки текстуры.
        /// Texture2D загруженная текстура.
        /// </returns>
        UniTask<(bool, Texture2D)> LoadTextureAsync(int width, int height, CancellationToken cancellationToken);
    }
}