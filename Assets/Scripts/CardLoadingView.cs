using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CardLoading
{
    public class CardLoadingView : MonoBehaviour, ICardLoadingView
    {
        public event Action<ICardLoader> DropdownChoice;
        public event Action ButtonLoadClick;
        public event Action ButtonCancelClick;

        [field: SerializeField]
        public Vector2Int PictureSize { get; private set; } = new(155, 205);

        [SerializeField]
        private Card cardPrefab;

        [SerializeField]
        private TMP_Dropdown dropdown;
        [SerializeField]
        private Button buttonLoad;
        [SerializeField]
        private Button buttonCancel;

        [SerializeField]
        private Transform cardsContainer;

        private readonly List<Card> cards = new();
        private List<ICardLoader> dropdownObjects;

        public void CreateCards(int count)
        {
            for (int i = 0; i < count; i++)
            {
                Card card = Instantiate(cardPrefab);
                cards.Add(card);
                card.transform.SetParent(cardsContainer, false);
            }
        }

        public async UniTask SetCardStateAsync(int cardIdx, CardState state, bool animate = true)
        {
            Card card = cards[cardIdx];
            await card.SetStateAsync(state, animate);
        }

        public CardState GetCardState(int cardIdx)
        {
            Card card = cards[cardIdx];
            return card.State;
        }

        public void SetCardPictureTexture(int cardIdx, Texture2D texture)
        {
            Card card = cards[cardIdx];
            card.SetPictureTexture(texture);
        }

        public void SetDropdownOptions(IEnumerable<KeyValuePair<string, ICardLoader>> options)
        {
            dropdown.options = options.Select(x => new TMP_Dropdown.OptionData(x.Key)).ToList();
            dropdownObjects = options.Select(x => x.Value).ToList();
        }

        public void SetDropdownAndButtonLoadInteractable(bool isInteractable)
        {
            dropdown.interactable = isInteractable;
            buttonLoad.interactable = isInteractable;
        }

        public void SetButtonCancelInteractable(bool isInteractable)
        {
            buttonCancel.interactable = isInteractable;
        }

        private void OnEnable()
        {
            dropdown.onValueChanged.AddListener(OnDropdownValueChanged);
            buttonLoad.onClick.AddListener(OnButtonLoadClick);
            buttonCancel.onClick.AddListener(OnButtonCancelClick);
        }

        private void OnDisable()
        {
            dropdown.onValueChanged.RemoveListener(OnDropdownValueChanged);
            buttonLoad.onClick.RemoveListener(OnButtonLoadClick);
            buttonCancel.onClick.RemoveListener(OnButtonCancelClick);
        }

        private void OnDropdownValueChanged(int value)
        {
            DropdownChoice?.Invoke(dropdownObjects[value]);
        }

        private void OnButtonLoadClick()
        {
            ButtonLoadClick?.Invoke();
        }

        private void OnButtonCancelClick()
        {
            ButtonCancelClick?.Invoke();
        }
    }
}
