﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using ILogger = UnityEngine.ILogger;

namespace CardLoading
{
    public class CardLoadingModel : ICardLoadingModel, IDisposable
    {
        private ICardLoadingView View { get; set; }
        private ITextureLoader TextureLoader { get; set; }
        private ILogger Logger { get; set; }

        public int CardsCount { get; private set; }

        private List<ICardLoader> CardLoaders { get; set; }
        private ICardLoader CurrentCardLoader { get; set; }
        private CancellationTokenSource loadingCancelationTokenSource;

        public CardLoadingModel(
            ICardLoadingView view,
            ITextureLoader textureLoader,
            IEnumerable<ICardLoader> cardsLoaders,
            ILogger logger,
            int cardsCount)
        {
            View = view;
            TextureLoader = textureLoader;
            CardsCount = cardsCount;
            CardLoaders = cardsLoaders.ToList();
            CurrentCardLoader = CardLoaders.ElementAtOrDefault(0);
            Logger = logger;

            Init();
        }

        private void Init()
        {
            View.DropdownChoice += View_DropdownChoice;
            View.ButtonLoadClick += View_ButtonLoadClick;
            View.ButtonCancelClick += View_ButtonCancelClick;

            View.SetDropdownOptions(CardLoaders.Select(x => KeyValuePair.Create(x.Name, x)));
            View.CreateCards(CardsCount);

            View.SetButtonCancelInteractable(false);
        }

        private void View_DropdownChoice(ICardLoader cardLoader)
        {
            SetCardLoader(cardLoader);
        }

        public void SetCardLoader(ICardLoader cardLoader)
        {
            CurrentCardLoader = cardLoader;
        }

        private void View_ButtonLoadClick()
        {
            LoadCardsAsync().Forget();
        }

        public async UniTask LoadCardsAsync()
        {
            View.SetDropdownAndButtonLoadInteractable(false);
            View.SetButtonCancelInteractable(true);

            loadingCancelationTokenSource = new();
            CancellationToken loadingCancelationToken =
                loadingCancelationTokenSource.Token;

            await CloseAllCardsAsync();

            try
            {
                await CurrentCardLoader.LoadAsync(this, View, loadingCancelationToken);
            }
            catch (OperationCanceledException ex)
            {
                Logger.Log(ex);
            }
            finally
            {
                View.SetDropdownAndButtonLoadInteractable(true);
                View.SetButtonCancelInteractable(false);

                loadingCancelationTokenSource.Dispose();
            }
        }

        private void View_ButtonCancelClick()
        {
            CancelLoading();
        }

        public void CancelLoading()
        {
            loadingCancelationTokenSource.Cancel();
        }

        private async UniTask CloseAllCardsAsync()
        {
            await AllCards().Select(i => View.SetCardStateAsync(i, CardState.Closed));
        }

        public IEnumerable<int> AllCards()
        {
            for (int i = 0; i < CardsCount; i++)
            {
                yield return i;
            }
        }

        public async UniTask LoadCardImageAsync(int cardIdx, CancellationToken cancellationToken)
        {
            var (success, texture) =
                await TextureLoader.LoadTextureAsync(
                    View.PictureSize.x,
                    View.PictureSize.y,
                    cancellationToken);

            if (success)
            {
                View.SetCardPictureTexture(cardIdx, texture);
            }
        }

        public void Dispose()
        {
            View.DropdownChoice -= View_DropdownChoice;
            View.ButtonLoadClick -= View_ButtonLoadClick;
            View.ButtonCancelClick -= View_ButtonCancelClick;
        }
    }
}
