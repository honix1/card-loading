using Cysharp.Threading.Tasks;
using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace CardLoading
{
    public class Card : MonoBehaviour
    {
        [SerializeField]
        private Transform front;
        [SerializeField]
        private Transform back;
        [SerializeField]
        private Image imagePicture;

        public CardState State { get; private set; } = CardState.None;

        private void Start()
        {
            imagePicture.sprite = null;
            SetStateAsync(CardState.Closed, false).Forget();
        }

        public void SetPictureTexture(Texture2D texture)
        {
            if (imagePicture.sprite != null)
                Destroy(imagePicture.sprite.texture);

            imagePicture.sprite = Sprite.Create(
                texture,
                new Rect(0.0f, 0.0f, texture.width, texture.height),
                new Vector2(0.5f, 0.5f),
                pixelsPerUnit: 100,
                extrude: 0,
                SpriteMeshType.FullRect);
        }

        public async UniTask SetStateAsync(CardState state, bool animate)
        {
            if (state == State)
                return;

            transform.DOComplete(withCallbacks: true);

            Sequence seq;

            switch (State, state)
            {
                case (CardState.None, CardState.Opened):
                case (CardState.Closed, CardState.Opened):
                    seq = TurnAnimation(back, front);
                    break;

                case (CardState.None, CardState.Closed):
                case (CardState.Opened, CardState.Closed):
                    seq = TurnAnimation(front, back);
                    break;

                default:
                    throw new Exception($"No animation to animate from {State} to {state}");
            }

            State = state;

            if (animate)
            {
                await seq.AsyncWaitForCompletion();
            }
            else
            {
                seq.Complete(withCallbacks: true);
            }

            Sequence TurnAnimation(Transform fromSide, Transform toSide)
            {
                return DOTween.Sequence()
                    .Append(fromSide.DOScaleX(0, .25f).SetEase(Ease.InCubic))
                    .AppendCallback(() =>
                    {
                        fromSide.gameObject.SetActive(false);
                        toSide.gameObject.SetActive(true);
                    })
                    .Append(toSide.DOScaleX(1, .25f).SetEase(Ease.OutCubic))
                    .Insert(0, transform.DOPunchRotation(Vector3.forward * -5, .5f, 5, 1))
                    .Insert(0, transform.DOPunchScale(transform.localScale * 0.25f, .5f, 1, 1))
                    .SetTarget(transform);
            }
        }
    }
}
