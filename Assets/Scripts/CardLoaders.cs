﻿using Cysharp.Threading.Tasks;
using System.Collections.Generic;
using System.Threading;

namespace CardLoading
{
    public static class CardLoadersUtility
    {
        public static List<ICardLoader> AllLoaders => new()
            {
                new OneByOneCardLoader(),
                new AllAtOnceCardLoader(),
                new WhenImageReadyCardLoader(),
            };
    }

    /// <summary>
    /// Последовательная загрузка. Грузится карта, затем отображается, затем грузится следующая карта.
    /// </summary>
    public class OneByOneCardLoader : ICardLoader
    {
        public string Name => "One By One";

        public async UniTask LoadAsync(
            ICardLoadingModel model,
            ICardLoadingView view,
            CancellationToken cancellationToken)
        {
            foreach (int i in model.AllCards())
            {
                await model.LoadCardImageAsync(i, cancellationToken);
                if (i < model.CardsCount - 1)
                    view.SetCardStateAsync(i, CardState.Opened).Forget();
                else
                    await view.SetCardStateAsync(i, CardState.Opened);
            }
        }
    }

    /// <summary>
    /// Паралельная загрузка. Отображение карт только когда все готовы.
    /// </summary>
    public class AllAtOnceCardLoader : ICardLoader
    {
        public string Name => "All At Once";

        public async UniTask LoadAsync(
            ICardLoadingModel model,
            ICardLoadingView view,
            CancellationToken cancellationToken)
        {
            await model.AllCards().Select(i => model.LoadCardImageAsync(i, cancellationToken));
            await model.AllCards().Select(i => view.SetCardStateAsync(i, CardState.Opened));
        }
    }

    /// <summary>
    /// Паралельная загрузка. Отображение каждой карты по готовности.
    /// </summary>
    public class WhenImageReadyCardLoader : ICardLoader
    {
        public string Name => "When Image Ready";

        public async UniTask LoadAsync(
            ICardLoadingModel model,
            ICardLoadingView view,
            CancellationToken cancellationToken)
        {
            await model.AllCards().Select(i =>
            {
                return model.LoadCardImageAsync(i, cancellationToken)
                    .ContinueWith(() => view.SetCardStateAsync(i, CardState.Opened));
            });
        }
    }
}
