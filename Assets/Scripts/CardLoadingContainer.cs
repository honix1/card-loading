using System.Collections.Generic;
using UnityEngine;

namespace CardLoading
{
    public class CardLoadingContainer : MonoBehaviour
    {
        [SerializeField]
        private int cardsCount = 1;

        void Start()
        {
            CardLoadingView view = GetComponent<CardLoadingView>();
            PicsumTextureLoader picsumTextureLoader = new();
            ILogger logger = Debug.unityLogger;

            new CardLoadingModel(
                view: view,
                textureLoader: picsumTextureLoader,
                cardsLoaders: CardLoadersUtility.AllLoaders,
                logger: logger,
                cardsCount);
        }
    }
}
