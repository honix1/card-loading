using Cysharp.Threading.Tasks;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace CardLoading.Tests
{
    public class CardLoadingTests
    {
        CardLoadingModel model;

        const int cardCount = 5;
        int setCardStateCallCount = 0;
        int loadTextureAsyncCallCount = 0;

        [SetUp]
        public void SetUp()
        {
            var cardLoadingViewMock = new Mock<ICardLoadingView>();
            cardLoadingViewMock
                .Setup(foo => foo.SetCardStateAsync(
                    It.IsInRange(0, cardCount - 1, Range.Inclusive),
                    It.IsAny<CardState>(),
                    It.IsAny<bool>()))
                .Callback(() => setCardStateCallCount++);

            var textureLoaderMock = new Mock<ITextureLoader>();
            textureLoaderMock
                .Setup(foo => foo.LoadTextureAsync(
                    It.IsAny<int>(),
                    It.IsAny<int>(),
                    It.IsAny<CancellationToken>()))
                .Callback(() => loadTextureAsyncCallCount++);

            model =
                new(
                    cardLoadingViewMock.Object,
                    textureLoaderMock.Object,
                    cardsLoaders: new List<ICardLoader> { },
                    new Mock<ILogger>().Object,
                    cardCount
                );
        }

        [TearDown]
        public void Cleanup()
        {
            model.Dispose();
        }

        [Test]
        public async Task CardLoadingCallCountTest()
        {
            var loaders = CardLoadersUtility.AllLoaders;

            foreach (ICardLoader cardLoader in loaders)
            {
                setCardStateCallCount = 0;
                loadTextureAsyncCallCount = 0;

                model.SetCardLoader(cardLoader);

                await model.LoadCardsAsync();

                Assert.AreEqual(cardCount, loadTextureAsyncCallCount);

                // close all then open all
                Assert.AreEqual(cardCount * 2, setCardStateCallCount);
            }
        }

        [Test]
        public async Task OneByOneCardLoaderTest()
        {
            var cardLoadingModelMock = new Mock<ICardLoadingModel>();
            var cardLoadingViewMock = new Mock<ICardLoadingView>();

            cardLoadingModelMock.Setup(foo => foo.AllCards()).Returns(new[] { 0, 1, 2 });
            cardLoadingModelMock.SetupGet(foo => foo.CardsCount).Returns(3);

            List<char> seq = new();

            cardLoadingModelMock
                .Setup(foo => foo.LoadCardImageAsync(0, It.IsAny<CancellationToken>()))
                .Returns(async () => await UniTask.Delay(100))
                .Callback(() => seq.Add('a'));
            cardLoadingModelMock
                .Setup(foo => foo.LoadCardImageAsync(1, It.IsAny<CancellationToken>()))
                .Returns(async () => await UniTask.Delay(200))
                .Callback(() => seq.Add('b'));
            cardLoadingModelMock
                .Setup(foo => foo.LoadCardImageAsync(2, It.IsAny<CancellationToken>()))
                .Returns(async () => await UniTask.Delay(0))
                .Callback(() => seq.Add('c'));

            cardLoadingViewMock
                .Setup(foo => foo.SetCardStateAsync(0, CardState.Opened, true))
                .Callback(() => seq.Add('d'));
            cardLoadingViewMock
                .Setup(foo => foo.SetCardStateAsync(1, CardState.Opened, true))
                .Callback(() => seq.Add('e'));
            cardLoadingViewMock
                .Setup(foo => foo.SetCardStateAsync(2, CardState.Opened, true))
                .Callback(() => seq.Add('f'));

            CancellationTokenSource cancellationTokenSource = new();
            CancellationToken cancellationToken = cancellationTokenSource.Token;

            OneByOneCardLoader loader = new();
            await loader.LoadAsync(cardLoadingModelMock.Object, cardLoadingViewMock.Object, cancellationToken);

            Assert.AreEqual("adbecf", new string(seq.ToArray()));

            cancellationTokenSource.Dispose();
        }

        [Test]
        public async Task AllAtOnceCardLoaderTest()
        {
            var cardLoadingModelMock = new Mock<ICardLoadingModel>();
            var cardLoadingViewMock = new Mock<ICardLoadingView>();

            cardLoadingModelMock.Setup(foo => foo.AllCards()).Returns(new[] { 0, 1, 2 });
            cardLoadingModelMock.SetupGet(foo => foo.CardsCount).Returns(3);

            List<char> seq = new();

            cardLoadingModelMock
                .Setup(foo => foo.LoadCardImageAsync(0, It.IsAny<CancellationToken>()))
                .Returns(async () => await UniTask.Delay(100))
                .Callback(() => seq.Add('a'));
            cardLoadingModelMock
                .Setup(foo => foo.LoadCardImageAsync(1, It.IsAny<CancellationToken>()))
                .Returns(async () => await UniTask.Delay(200))
                .Callback(() => seq.Add('b'));
            cardLoadingModelMock
                .Setup(foo => foo.LoadCardImageAsync(2, It.IsAny<CancellationToken>()))
                .Returns(async () => await UniTask.Delay(0))
                .Callback(() => seq.Add('c'));

            cardLoadingViewMock
                .Setup(foo => foo.SetCardStateAsync(0, CardState.Opened, true))
                .Callback(() => seq.Add('d'));
            cardLoadingViewMock
                .Setup(foo => foo.SetCardStateAsync(1, CardState.Opened, true))
                .Callback(() => seq.Add('e'));
            cardLoadingViewMock
                .Setup(foo => foo.SetCardStateAsync(2, CardState.Opened, true))
                .Callback(() => seq.Add('f'));

            CancellationTokenSource cancellationTokenSource = new();
            CancellationToken cancellationToken = cancellationTokenSource.Token;

            AllAtOnceCardLoader loader = new();
            await loader.LoadAsync(cardLoadingModelMock.Object, cardLoadingViewMock.Object, cancellationToken);

            Assert.AreEqual("abcdef", new string(seq.ToArray()));

            cancellationTokenSource.Dispose();
        }

        [Test]
        public async Task WhenImageReadyCardLoaderTest()
        {
            var cardLoadingModelMock = new Mock<ICardLoadingModel>();
            var cardLoadingViewMock = new Mock<ICardLoadingView>();

            cardLoadingModelMock.Setup(foo => foo.AllCards()).Returns(new[] { 0, 1, 2 });
            cardLoadingModelMock.SetupGet(foo => foo.CardsCount).Returns(3);

            List<char> seq = new();

            cardLoadingModelMock
                .Setup(foo => foo.LoadCardImageAsync(0, It.IsAny<CancellationToken>()))
                .Returns(async () => await UniTask.Delay(100))
                .Callback(() => seq.Add('a'));
            cardLoadingModelMock
                .Setup(foo => foo.LoadCardImageAsync(1, It.IsAny<CancellationToken>()))
                .Returns(async () => await UniTask.Delay(200))
                .Callback(() => seq.Add('b'));
            cardLoadingModelMock
                .Setup(foo => foo.LoadCardImageAsync(2, It.IsAny<CancellationToken>()))
                .Returns(async () => await UniTask.Delay(0))
                .Callback(() => seq.Add('c'));

            cardLoadingViewMock
                .Setup(foo => foo.SetCardStateAsync(0, CardState.Opened, true))
                .Callback(() => seq.Add('d'));
            cardLoadingViewMock
                .Setup(foo => foo.SetCardStateAsync(1, CardState.Opened, true))
                .Callback(() => seq.Add('e'));
            cardLoadingViewMock
                .Setup(foo => foo.SetCardStateAsync(2, CardState.Opened, true))
                .Callback(() => seq.Add('f'));

            CancellationTokenSource cancellationTokenSource = new();
            CancellationToken cancellationToken = cancellationTokenSource.Token;

            WhenImageReadyCardLoader loader = new();
            await loader.LoadAsync(cardLoadingModelMock.Object, cardLoadingViewMock.Object, cancellationToken);

            Assert.AreEqual("abcfde", new string(seq.ToArray()));

            cancellationTokenSource.Dispose();
        }
    }
}