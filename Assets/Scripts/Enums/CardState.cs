﻿namespace CardLoading
{
    public enum CardState
    {
        None,
        Closed,
        Opened,
    }
}
