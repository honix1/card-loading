﻿using Cysharp.Threading.Tasks;
using System;
using System.Threading;
using UnityEngine;
using UnityEngine.Networking;

namespace CardLoading
{
    public class PicsumTextureLoader : ITextureLoader
    {
        private const string picsumImageSourceUrl = "https://picsum.photos/{0}/{1}";

        public async UniTask<(bool, Texture2D)> LoadTextureAsync(
            int width,
            int height,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            string url = string.Format(picsumImageSourceUrl, width, height);

            using UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);

            try
            {
                await request.SendWebRequest().WithCancellation(cancellationToken);
            }
            catch (UnityWebRequestException ex)
            {
                Debug.LogException(ex);
                return (false, null);
            }

            if (request.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError(request.error);
                return (false, null);
            }

            cancellationToken.ThrowIfCancellationRequested();

            // TODO: async GetContent?
            Texture2D texture = DownloadHandlerTexture.GetContent(request);

            if (texture == null)
            {
                Debug.LogError("Get content of request as Texture failed");
                return (false, null);
            }

            return (true, texture);
        }
    }
}
